package BibliotecaApp.model;

import org.junit.*;

import static org.junit.Assert.*;

public class CarteTest {
    private Carte c1;
    private Carte c2;
    private Carte c3;
    private Carte c4;

    //se executa automat inaintea fiecarei metode de test
    @Before
    public void setUp() {
        c1 = new Carte();//1,"Povesti", 198);
        c1.setTitlu("Povesti");
        c2 = new Carte();//2,"Povestiri",170);
        c2.setTitlu("Povestiri");
        c3 = new Carte();
        c4 = null;

        System.out.println("Before test");
    }

    //se executa automat dupa fiecarei metoda de test
    @After
    public void tearDown() {
        c1 = null;
        c2 = null;
        c3 = null;

        System.out.println("After test");
    }

    @Test
    public void testGetTitlu() {
        assertEquals("Povesti", c1.getTitlu());
    }

    @Test (expected=NullPointerException.class)
    public void testGetTitlu2() {
        assertEquals("Povesti", c4.getTitlu());
    }

    @Test (timeout=100) //asteapta 10 milisecunde
    public void testFictiv(){
        try {
            Thread.sleep (101);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    @Test
    public void testConstructor(){
        assertNotEquals("verificam daca s-a creat cartea 1",c1,null);
    }

    @BeforeClass
    public static void setUpAll(){
        System.out.println("Before All tests - at the beginning of the Test Class");
    }

    @AfterClass
    public static void tearDownAll(){
        System.out.println("After All tests - at the end of the Test Class");
    }
}